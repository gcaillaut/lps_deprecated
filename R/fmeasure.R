precision <- function(found, expected, inter = NULL) {
  if (is.null(inter)) {
    inter <- sum(found & expected)
  }
  inter / sum(found)
}

recall <- function(found, expected, inter = NULL) {
  if (is.null(inter)) {
    inter <- sum(found & expected)
  }
  inter / sum(expected)
}

fmeasure <- function(found, expected) {
  inter <- sum(found & expected)
  p <- precision(found, expected, inter = inter)
  r <- recall(found, expected, inter = inter)
  fmeasure_from_pr(p, r)
}

fmeasure_from_pr <- function(p, r) {
  denominator <- p + r
  fm <- 
    if (identical(denominator, 0)) {
      0
    }
    else {
      (2 * p * r) / denominator
    }

  structure(
    c(precision = p, recall = r, fmeasure = fm),
    class = "fmeasure"
  )
}

format.fmeasure <- function(x, digits = 3L) {
  paste(c("P:", "R:", "F:"), round(x, digits = digits), collapse = "  ")
}

print.fmeasure <- function(x, ...) {
  cat(format(x, ...), "\n")
}

Ops.fmeasure <- function(e1, e2) {
  get(.Generic)(as.numeric(e1), as.numeric(e2))
}

as.double.fmeasure <- function(x) {
  x[["fmeasure"]]
}
