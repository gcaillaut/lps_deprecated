equivalence_classes <- function(adj) {
  classes <- list()
  rows <- seq_len(nrow(adj))

  while(length(rows) > 0) {
    i <- rows[[1]]
    same_rows <- apply(adj[rows, , drop = FALSE], 1, identical, adj[i, ])
    classes <- c(classes, list(rows[same_rows]))
    rows <- rows[!same_rows]
  }

  class_count <- length(classes)
  equivalence_matrix <- matrix(FALSE, nrow = class_count, ncol = ncol(adj))
  closure_matrix <- matrix(FALSE, nrow = class_count, ncol = ncol(adj))

  for (i in seq_len(class_count)) {
    current_class <- classes[[i]]
    equivalence_matrix[i, current_class] <- TRUE
    closure_matrix[i, ] <- adj[current_class[[1]], ]
  }

  list(classes = equivalence_matrix, closed_sets = closure_matrix)
}

# pretopological_structuring <- function(space) {
#   closed_sets <- elementary_closed_sets(space)
#   equivalence <- equivalence_classes(closed_sets)
# }

# transitive_closure <- function(adj) {
#   stopifnot(identical(nrow(adj), ncol(adj)))
#   n <- ncol(adj)
#   vertices <- seq_len(n)
#   for (k in vertices) {
#     for (i in vertices) {
#       adj[i, ] <- pmax(adj[i, ], adj[i, k] * adj[k, ])
#     }
#   }
#   adj
# }

# dag_transitive_reduction <- function(adj, closure = NULL) {
#   paths <-
#     if(is.null(closure)) transitive_closure(adj)
#     else closure

#   diag(adj) <- FALSE
#   diag(paths) <- FALSE
#   adj & !(adj %*% paths)
# }

pspace_to_graph <- function(space) {
  adj <- diag(length(space$universe))
  for (i in seq_along(space$universe)) {
    adj[i, ] <- pseudo_closure(space, adj[i, ])
  }
  adj
}